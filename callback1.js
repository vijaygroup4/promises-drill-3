//importing fs and path modules
const fs = require("fs");
const path = require("path");

//function that returns board details by id
function problem1(id, callback) {
  return new Promise((resolve, reject) => {
    //stopping the execution for 2 sec
    setTimeout(() => {
      let boardsFilePath = path.join(__dirname, "boards_1.json");
      //reading the boards json file
      fs.readFile(boardsFilePath, "utf-8", (error, boardsData) => {
        if (error) {
          reject(error);
        }

        try {
          let boards = JSON.parse(boardsData);
          //finding the board
          let board = boards.find((board) => {
            return board.id === id;
          });

          resolve(board);
        } catch (error) {
          reject(error);
        }
      });
    }, 2000);
  });
}

//exporting the above function
module.exports = problem1;
