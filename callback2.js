//importing both fs and path modules
const fs = require("fs");
const path = require("path");

//function that return lists by id
function problem2(id) {
  return new Promise((resolve, reject) => {
    //stopping the execution for 2 sec
    setTimeout(() => {
      let listsFilePath = path.join(__dirname, "lists_1.json");
      //reading the lists json file
      fs.readFile(listsFilePath, "utf-8", (error, objectData) => {
        if (error) {
          reject(error);
        }

        try {
          let object = JSON.parse(objectData);
          //getting the list
          let list = object[id];
          resolve(list);
        } catch (error) {
          reject(error);
        }
      });
    }, 2000);
  });
}

//exporting the above function
module.exports = problem2;
