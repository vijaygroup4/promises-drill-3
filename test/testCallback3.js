//importing problem3 function
const problem3 = require("../callback3");

let listId = "qwsa221";
//executing problem3 function
async function test() {
  try {
    let result = await problem3(listId);
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}

test();
