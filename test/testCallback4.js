//importing problem4 function
const problem4 = require("../callback4");

//executing problem4 function
async function test() {
  try {
    let message = await problem4();
    console.log(message);
  } catch (error) {
    console.log(error);
  }
}

test();
