//importing problem6 function
const problem6 = require("../callback6");

//executing problem6 function
async function test() {
  try {
    let message = await problem6();
    console.log(message);
  } catch (error) {
    console.log(error);
  }
}

test();
