//import problem1 function
const problem1 = require("../callback1");

let boardId = "abc122dc";
//executing the problem1 function
async function test() {
  try {
    let result = await problem1(boardId);
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}

test();
