//importing problem5 function
const problem5 = require("../callback5");

//executing problem5 function
async function test() {
  try {
    let message = await problem5();
    console.log(message);
  } catch (error) {
    console.log(error);
  }
}

test();
