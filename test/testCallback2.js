//importing problem2 function
const problem2 = require("../callback2");

let boardId = "abc122dc";
//executing problem2 function
async function test() {
  try {
    let result = await problem2(boardId);
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}

test();
