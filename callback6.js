//importing both fs and path modules
const fs = require("fs");
const path = require("path");

//importing previous functions
const problem1 = require("./callback1");
const problem2 = require("./callback2");
const problem3 = require("./callback3");

//function that performs:
// Get information from the Thanos boards
// Get all the lists for the Thanos board
// Get all cards for all lists simultaneously
function problem6(callback) {
  return new Promise((resolve, reject) => {
    //pausing the execution for 2sec
    setTimeout(() => {
      let boardsFilePath = path.join(__dirname, "boards_1.json");
      //reading the boards file
      fs.readFile(boardsFilePath, "utf-8", async (boardsError, boardsData) => {
        if (boardsError) {
          reject(boardsError);
        }

        try {
          let boards = JSON.parse(boardsData);
          //filtering the boards for Thanos
          let filteredBoards = boards.filter((board) => {
            return board.name === "Thanos";
          });
          let boardId = filteredBoards[0].id;

          //getting the Thanos information
          try {
            let board = await problem1(boardId);
            if (board) {
              console.log("board=", board);
            } else {
              console.log("No board found");
            }
          } catch (error) {
            reject(error);
          }

          //getting the lists of Thanos
          try {
            let list = await problem2(boardId);
            if (list) {
              console.log("list=", list);
            } else {
              console.log("No list found");
            }
          } catch (error) {
            reject(error);
          }

          let listsFilePath = path.join(__dirname, "lists_1.json");
          //reading the lists json file
          fs.readFile(listsFilePath, "utf-8", async (listsError, listsData) => {
            if (listsError) {
              reject(listsError);
            }

            let listsObject = JSON.parse(listsData);
            let keysArray = Object.keys(listsObject);

            let allPromises = [];
            //For each list id,printing the cards details
            keysArray.forEach((innerId) => {
              let innerArray = listsObject[innerId];
              innerArray.forEach((innerObject) => {
                let requiredId = innerObject.id;
                let innerName = innerObject.name;
                //printing the cards details
                let promise = problem3(requiredId).then((cards) => {
                  if (cards) {
                    console.log(`${innerName}: ${JSON.stringify(cards)}`);
                  } else {
                    console.log(`${innerName}: No cards found`);
                  }
                });

                allPromises.push(promise);
              });
            });

            //waiting for all the promises to resolve
            try {
              await Promise.allSettled(allPromises);
              resolve("all operations done");
            } catch (error) {
              reject(error);
            }
          });
        } catch (error) {
          reject(error);
        }
      });
    }, 2000);
  });
}

// exporting the above function
module.exports = problem6;
