//importing both fs and path modules
const fs = require("fs");
const path = require("path");

//importing previous functions
const problem1 = require("./callback1");
const problem2 = require("./callback2");
const problem3 = require("./callback3");

//function that performs:
// Get information from the Thanos boards
// Get all the lists for the Thanos board
//Get all cards for the Mind and Space lists simultaneously
function problem5(callback) {
  return new Promise((resolve, reject) => {
    //pausing the execution for 2 sec
    setTimeout(() => {
      let boardsFilePath = path.join(__dirname, "boards_1.json");
      //reading the boards file
      fs.readFile(boardsFilePath, "utf-8", async (boardsError, boardsData) => {
        if (boardsError) {
          reject(boardsError);
        }

        try {
          let boards = JSON.parse(boardsData);
          //filtering for Thanos id
          let filteredBoards = boards.filter((board) => {
            return board.name === "Thanos";
          });
          let boardId = filteredBoards[0].id;

          //getting the Thanos details
          try {
            let board = await problem1(boardId);
            if (board) {
              console.log("board=", board);
            } else {
              console.log("No board found");
            }
          } catch (error) {
            reject(error);
          }

          //getting lists of Thanos
          try {
            let list = await problem2(boardId);
            if (list) {
              console.log("list=", list);
            } else {
              console.log("No list found");
            }
          } catch (error) {
            reject(error);
          }

          let listsFilePath = path.join(__dirname, "lists_1.json");
          //reading lists file
          fs.readFile(listsFilePath, "utf-8", async (listsError, listsData) => {
            if (listsError) {
              reject(listsError);
            }

            let listsObject = JSON.parse(listsData);
            let keysArray = Object.keys(listsObject);

            let requiredIdOfMind = "";
            let requiredIdOfSpace = "";
            //filtering for Mind and Space ids
            keysArray.forEach((innerId) => {
              let innerArray = listsObject[innerId];
              innerArray.forEach((object) => {
                if (object.name === "Mind") {
                  requiredIdOfMind = object.id;
                }
                if (object.name === "Space") {
                  requiredIdOfSpace = object.id;
                }
              });
            });

            //getting the cards of Mind
            try {
              let cardsOfMind = await problem3(requiredIdOfMind);
              if (cardsOfMind) {
                console.log("cards of Mind=", cardsOfMind);
              } else {
                console.log("Mind cards not found");
              }
            } catch (error) {
              reject(error);
            }

            //getting the cards of Space
            try {
              let cardsOfSpace = await problem3(requiredIdOfSpace);
              if (cardsOfSpace) {
                console.log("cards of Space=", cardsOfSpace);
                resolve("all operations done");
              } else {
                console.log("Space cards not found");
              }
            } catch (error) {
              reject(error);
            }
          });
        } catch (error) {
          reject(error);
        }
      });
    }, 2000);
  });
}

//exporting the above function
module.exports = problem5;
