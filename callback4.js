//importing both fs and path modules
const fs = require("fs");
const path = require("path");

//importing problem2 and problem3 functions
const problem1 = require("./callback1");
const problem2 = require("./callback2");
const problem3 = require("./callback3");

//function that performs:
//Get information from the Thanos boards
//Get all the lists for the Thanos board
//Get all cards for the Mind list simultaneously
function problem4() {
  return new Promise((resolve, reject) => {
    //stopping the execution for 2sec
    setTimeout(() => {
      let boardsFilePath = path.join(__dirname, "boards_1.json");
      //reading the boards file
      fs.readFile(boardsFilePath, "utf-8", async (boardsError, boardsData) => {
        if (boardsError) {
          reject(boardsError);
        }

        try {
          let boards = JSON.parse(boardsData);
          //filtering for Thanos id
          let filteredBoards = boards.filter((board) => {
            return board.name === "Thanos";
          });
          let boardId = filteredBoards[0].id;

          //getting the Thanos information
          try {
            let board = await problem1(boardId);
            if (board) {
              console.log("board=", board);
            } else {
              console.log("No board found");
            }
          } catch (error) {
            reject(error);
          }

          //using previously defined problem2 function for getting lists
          try {
            let list = await problem2(boardId);
            if (list) {
              console.log("list=", list);
            } else {
              console.log("No list found");
            }
          } catch (error) {
            reject(error);
          }

          let listsFilePath = path.join(__dirname, "lists_1.json");
          //reading the lists file
          fs.readFile(listsFilePath, "utf-8", async (listsError, listsData) => {
            if (listsError) {
              reject(listsError);
            }

            let listsObject = JSON.parse(listsData);
            let keysArray = Object.keys(listsObject);

            let requiredId = "";
            //filtering for Mind id
            keysArray.forEach((innerId) => {
              let innerArray = listsObject[innerId];
              innerArray.forEach((object) => {
                if (object.name === "Mind") {
                  requiredId = object.id;
                }
              });
            });

            //using previously defined problem3 function for getting cards
            try {
              let cards = await problem3(requiredId);
              if (cards) {
                console.log("cards=", cards);
                resolve("all operations done");
              } else {
                console.log("No cards found");
              }
            } catch (error) {
              reject(error);
            }
          });
        } catch (error) {
          reject(error);
        }
      });
    }, 2000);
  });
}

//exporting the above function
module.exports = problem4;
