//importing both fs and path modules
const fs = require("fs");
const path = require("path");

//function that return cards by id
function problem3(id, callback) {
  return new Promise((resolve, reject) => {
    //stopping the execution for 2 sec
    setTimeout(() => {
      let cardsFilePath = path.join(__dirname, "cards_1.json");
      //reading the cards json file
      fs.readFile(cardsFilePath, "utf-8", (error, cardsData) => {
        if (error) {
          reject(error);
        }
        try {
          let cardsObject = JSON.parse(cardsData);
          //getting the card
          let card = cardsObject[id];
          resolve(card);
        } catch (error) {
          reject(error);
        }
      });
    }, 2000);
  });
}

//exporting the above function
module.exports = problem3;
